# FilmChain Technology

## Platform Design

The platform design gravitates around 3 key concepts:

* Agility
* Scalability
* Transparency

To that end, we chose the following technologies for our platform

### Backend - Database

* [Graphql](https://graphql.org/) - Our API query language
* [Prisma](https://www.prisma.io/) - Database ORM
* [Express](https://expressjs.com/) - Backend framework

Having a graphql API makes frontend - backend integration seemingless so that as our team grows, the developer experience stays smooth.

### Frontend

* [React](https://reactjs.org/) - The main frontend library
* [Apollo](https://www.apollographql.com/docs/react/) - Frontend client for graphql
* [Styled Components](https://www.styled-components.com/) - CSS in JS

React as by far the largest online community and is considered the goto library for frontend development. Styled components are quickly replacing traditional [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS) and make design implementation quicker and easier. Apollo is an open-source project to run a graphql client on the frontend.


### Blockchain

* [Ethereum](https://www.ethereum.org/) - Our Blockchain technology
* [Solidity](https://solidity.readthedocs.io/en/v0.5.3/) - Ethereum coding language
* [OpenZeppelin Solidity](https://github.com/OpenZeppelin/openzeppelin-solidity) - Library to build secure and test smart contracts

As the database in still under heavy development, we currently are running an Ethereum blockchain on a private network. That way we can compare our algorithm results against its javascript version running on our backend server. In the future, we aim to run the smart contracts on a permissioned/public blockchain so to make clients able, if they wish so, to trade their blockchains assets. 

## Database Design

In what follows, we present the database entities that are relevant to the distribution of revenues. These entities are substantially the same whether we look at the blockchain or our server. The difference is mainly found in the implementation: on the server we can use all the javascript synthetic sugar that makes code much more readable and concise.
Any code snippet in the following parts will therefore be reported in Javascript. 

### General Structure

_Collection_ _Account_ _Management_ _Agreement_) aka CAMA - the legal contract underlying the distribution of cash flows - spans in two dimensions. 

* Vertically, it has hierarchical structure("the waterfall") meaning that pools of stakeholders have precedence/seniority over others
* Horizontally, it is divided in groups which might represent anything from a single country, a pool of countries or festivals

They main entities involved in the distribution are:

1. Stakeholder
2. Tranche
3. Group
4. Collection Source
5. Distribution
6. Round



#### Stakeholder

  Anyone who will receive any part of the revenue. Their most important attributes are:

- **fixedAmountToReceive**: total amount they are allowed to recoup from start to finish(ex: 
$10,000 might be the cap for a small investor)
- **amountReceivedSoFar:** between 0 and fixedAmountToReceive
- **type**: commission/grossReceipts/equity/normal stakeholder
- **Percentage**: some stakeholders recoup based on a percentage of the incoming funds

The **types** of stakeholder we currently account for are:

- **commission**: as soon as *amountReceivedSoFar* ≥ *fixedAmountToReceive*, they will receive a percentage of any future inflow
- **grossReceipts**: receives a percentage of incoming flow of money as long as *amountReceivedSoFar* < *fixedAmountToReceive*
- **equity**: as soon as the movie breaks even, they receive a percentage of incoming flows

NB: all of these types are mutually exclusives(you cannot have a commission receiver who happens to be also a gross receiver). Hence, a physical person which happens two have two or more of these roles will have multiple stakeholders associated

Code snippets:
```
type TrancheStakeholder {
  id: ID! @unique
  tranche: Tranche! @relation(name: "StakeholdersWithPercentages")
  stakeholder: Stakeholder! @relation(name: "TrancheStakeholder")
  percentage: Float! @default(value: 0)
}

enum StakeholderType {
  EQUITY
  COMMISSION
  COLLECTEDGROSSRECEIPTS
  FIXEDAMOUNT
  OTHER
}

type Stakeholder {
  id: ID! @unique
  user: User! @relation(name: "UserStakeholder")
  trancheStakeholders: [TrancheStakeholder]!
    @relation(name: "TrancheStakeholder", onDelete: CASCADE)
  fixedAmountToRecoup: Float!
  amountRecouped: Float! @default(value: 0)
  type: StakeholderType!
  totalPercentageReceived: Float! @default(value: 0)
  hasGreenlightedCama: Boolean! @default(value: false)
  isParty: Boolean! @default(value: false)
  description: String @default(value: "")
  beneficiaryClassTokenAddress: String @default(value: "")
}
```


To see more, please [visit our graphql playground and click on DOCS](https://filmchain-staging-b76313c1e9.herokuapp.com/filmchain-staging/staging)


#### Tranche

Tranches are used as stakeholders containers. In each tranche, stakeholders have the same seniority but they might differ in their types or amount to recoup.
To account for the fact that each stakeholder might have different percentage in different tranches, we represent each tranche/stakeholder relation using another entity which we originally have called TrancheStakeholder.
For example, John Doe, a sales agent, might recoup 0.2% in all festivals but 10% in Cannes. We might create two different stakeholders but what if the CAMA stipulates that he share the amount recouped accross these different groups? That is when TrancheStakeholder relation is necessary.

Code snippets:
```
type TrancheStakeholder {
  id: ID! @unique
  tranche: Tranche! @relation(name: "StakeholdersWithPercentages")
  stakeholder: Stakeholder! @relation(name: "TrancheStakeholder")
  percentage: Float! @default(value: 0)
}

type Tranche {
  id: ID! @unique
  displayName: String!
  group: Group! @relation(name: "WaterfallTranches")
  position: Int!
  cap: Float @default(value: 0)
  createdAt: DateTime!
  updatedAt: DateTime!
  stakeholders: [TrancheStakeholder]!
    @default(value: [])
    @relation(name: "StakeholdersWithPercentages", onDelete: CASCADE) #check if fine
}
```

#### Group

A Group can be anything from a group of territories to festival. Moreover, a group can countain sub-groups which are themselves groups.
Our CAMA representation can potentially accomodate any possible kind of agreement between parties, in a way that has never been possible until now.

#### Collection Source

```
type CollectionSource {
  id: ID! @unique
  kind: CollectionSourceKind!
  firstName: String!
  lastName: String!
  email: String!
  phone: String!
  group: Group! @relation(name: "CollectionSourceGroup")
  company: Company @relation(name: "CollectionSourceCompany", onDelete: CASCADE)
  movie: Movie! @relation(name: "MovieCollectionSource")
}
```

Typically, collection sources are entities charged to collect revenue and send it into the movie/project bank account.
We link each collection source to a Group. 

#### Distribution

When a collection source sends some revenue, we create a Transaction object which triggers the creation of a distribution object in the database. This distribution is uniquely identified by its collection source and is therefore linked to a movie and, more precisely, to a group in that movie.
Our distribution algorithm will create different rounds and fill the distribution object with them.
That distribution item is then available to be seen on our platform (and later on the blockchain) so that a dispute can be raised.
Since we do not modify our Stakeholders until everyone agrees on the distribution or a predefined time elapsed(such as 2 days), we can revert both databases without compromising the integrity of the data (the dispute invalidate the blockchain distribution item created). 

```
type Distribution {
  id: ID! @unique
  amountToDistribute: Float!
  approved: Boolean! @default(value: false)
  movie: Movie!
  transaction: Transaction!
  group: Group!
  # nextTranchePosition: int! @default(value: 1)
  rounds: [Round]! @default(value: [])
  filmchainStakeholderAmount: Float! @default(value: 0)
  filmchainStakeholderPercentage: Float! @default(value: 0.07)
  createdAt: DateTime!
  updatedAt: DateTime!
}
```


#### Round

A round can be thought as the most atomic element of a distribution. At its core, it is a stakeholder and the amount he recouped. The sum of all rounds equal the amount of money received from the transaction.


```
type Round {
  id: ID! @unique
  type: RoundType!
  stakeholder: Stakeholder!
  amountDistributed: Float!
  percentageApplied: Float @default(value: 0)
}
```

We have now the necessary tools to dive into the distribution algorithm design.

## Distribution Algorithm

CAMA is based on two core concepts: 

* **Pari Passu** - every stakeholder in a tranche must recoup at the same time
* **Pro Rata** - every stakeholder in a tranche recoups proportionally to the total amount that must be recouped in that given tranche 

### Implementation

The following code is a lean version of what **FilmChain** distribution looks like.


```
async function distribute({ transactionId }) {
  // 1. find Transaction using transactionId, throw error if transaction not found or not assigned
  let transaction = await findTransaction({ transactionId })
  const { collectionSource } = transaction

  // 2. check group does not have percentage errors
  checkPercentageErrors({ group: collectionSource.group })

  // 3. check equity does not have percentage errors
  const equityGroup = await findEquityGroup({
    camaId: collectionSource.movie.cama.id
  })

  // 4. start distribution
  let amountDistributed = startDistribution({
    equityGroup,
    transactionId,
    amountToDistribute: transaction.amountValue,
    collectionSource
  })

  // 5. complete distribution
  transaction = await completeDistribution({ transactionId })

  return amountDistributed
}
```

```
async function startDistribution({
  transactionId,
  equityGroup,
  collectionSource,
  amountToDistribute
}) {
  // 1. compute amount filmchain 
  const filmchainStakeholder = computeFilmchainStakeholderAmount({
    amountToDistribute,
    filmchainStakeholder: collectionSource.movie.filmchainStakeholder,
    totalAmountDistributed: collectionSource.movie.cama.totalAmountDistributed
  })

  // 2. create a distribution 
  const distribution = await createDistribution({
    movieId: collectionSource.movie.id,
    transactionId,
    groupId: collectionSource.group.id,
    amountToDistribute,
    filmchainStakeholderAmount: filmchainStakeholder.amount,
    filmchainStakeholderPercentage: filmchainStakeholder.percentage
  })

  // 3. update amount to distribute
  amountToDistribute -= filmchainStakeholder.amount

  // 4. start group distribution
  let amountDistributed = 0
  if (collectionSource.group.type === 'SUPERGROUP') {
    // 4. a. distribute in each subgroup using appropriate percentage
    for (group of collectionSource.group.groups) {
      amountDistributed += await distributeInGroup({
        tranches: group.tranches,
        distributionId: distribution.id,
        amountToDistribute: amountToDistribute * group.percentage
      })
    }
  } else {
    amountDistributed += await distributeInGroup({
      tranches: collectionSource.group.tranches,
      distributionId: distribution.id,
      amountToDistribute
    })
  }

  //  5. if anything left, distribute to equity
  if (amountDistributed < amountToDistribute) {
    let equityAmountToDistribute = amountToDistribute - amountDistributed
    amountDistributed += await distributeInEquity({
      amountToDistribute: equityAmountToDistribute,
      distributionId: distribution.id,
      totalAmountDistributed: collectionSource.movie.cama.totalAmountDistributed,
      tranches: equityGroup.tranches
    })
  }

  return amountDistributed + distribution.filmchainStakeholderAmount
}
```
```
async function distributeInGroup({
  tranche,
  distributionId,
  amountToDistributeInTranche
}) {
  // 1. find next tranche to distribute into
  // 2. distribute
  // 3. update Amount to distribute
  // if any amount left or tranches GOTO 1.
  // return amount distributed
```

```
async function distributeInTranche({
  tranche,
  distributionId,
  amountToDistributeInTranche
}) {

  // 1. divide stakeholders into sets
  // 2. nothing to distribute
  if (allRecouped && !someCommissions) return amountDistributed

  // 3. switch cases
  // 3.1 distribute to commissions
  if (allRecouped && someCommissions) {
    return await distributeCommissions({
      tranche,
      amountToDistributeInTranche,
      distributionId
    })
  }
  // 3.2 distribute normally
  
  // ...
  return amountDistributed
}
```

### Examples

The following are simple examples that we also tested on our platform

#### Example 1
| Stakeholder | Tranche | Amount To Recoup | Amount Recouped | Percentage |
|:-----------:|:-------:|:----------------:|:---------------:|:----------:|
| A - normal  | 1       | $40              | $0              | null       |
| B - normal  | 1       | $20              | $0              | null       |

Here A has $40 to recoup and B has $20 to recoup

if $30 flow in:
 * By Pari Passu, both will recoup
 * By Pro Rata A will receive 2/3 of the amount and B will receive 1/3
 Thus A receives $20 and B receives $10

**NB**: *unless a stakeholder has a commission, the total amount he receive has to be below his total amount to receive*

Therefore, if instead of $30 we distribute $120, A will receive $40 and B will receive $20. The amount left will be distributed in the following tranche


#### Example 2
| Stakeholder | Tranche | Amount To Recoup | Amount Recouped | Percentage |
|:-----------:|:-------:|:----------------:|:---------------:|:----------:|
| A-commission| 1       | $10              | $0              | 10%        |
| B - normal  | 2       | $10              | $0              | null       |
| C - normal  | 2       | $10              | $0              | null       |


**if $25 are distributed:**

* Round 1: A gets $10
* Round 2: A gets $ 0.10 * (25 - 10) = 1.5 i.e. he gets a commission of what is remaining after he received his total amount
* Round 3: B gets $ 13.5 / 2 = 6.25
* Round 4: C gets $ 13.5 / 2 = 6.25



#### Example 3
Now A is of type Gross Receipt

| Stakeholder | Tranche | Amount To Recoup | Amount Recouped | Percentage |
|:-----------:|:-------:|:----------------:|:---------------:|:----------:|
| A- gross Receiver    | 1       | $10              | $0              | 10%        |
| B - normal  | 2       | $10              | $0              | null       |
| C - normal  | 2       | $10              | $0              | null       |



**if $25 are distributed:**

* Round 1: A gets $ 0.10 * 25 = $ 2.5
* Round 2: B gets $10
* Round 3: C gets $10

[example 3: all in same set; A has a gross of 10%, B commission 10%](https://www.notion.so/d1ffe917633744e6a97f17da06a0b993)
#### Example 4 
Everyone in the same tranche, B has commission 10%

| Stakeholder | Tranche | Amount To Recoup | Amount Recouped | Percentage |
|:-----------:|:-------:|:----------------:|:---------------:|:----------:|
| A- gross    | 1       | $10              | $0              | 10%        |
| B - comm    | 1       | $10              | $0              | 10%        |
| C - normal  | 1       | $10              | $0              | null       |

**if $25 are distributed:**

* Round 1: A gets 0.10 * $25 = $ 2.5
* Round 2: B gets $10
* Round 3: C gets $10
* Round 4: B gets $0.25 (he recouped is amount hence he has to get 10% commission on what is left)

 $2.25 are left to next tranche