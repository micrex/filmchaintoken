contract Cama {

    address filmchainAddress;
    mapping(string => address) groups;
    address revenueAddress;
    address beneficiaryAddress;
    address equityTrancheAddress;

    uint BASE = 10**12;

    address private owner;

    modifier onlyOwner {
      require(msg.sender == owner);
      _;
    }

    /**
     * DISTRIBUTE FUNDS
     *
     */
    function distributeFunds(string territory, uint funds) onlyOwner public returns (bo


    /**
     * ADD GROUP
     *
     */
    function addGroup(string group) onlyOwner public;

    /**
     * ADD TRANCHE
     *
     */
    function addTranche(string destinationCountry) onlyOwner public;

    /**
     * ADD STAKEHOLDER
     *
     */
    function addStakeholder( address userAddress, string destinationGroup, uint trancheIndex, uint beneficiaryClassTokenId, uint fixedAmountToRecoup, map(string => uint)) onlyOwner public;


    /*********************************************

    GETTERS

    *********************************************/

    /**
     * GET REVENUE ADDRESS
     *
     */
    function getRevenueAddress() public view returns (address) {
        return revenueAddress;
    }

    /**
     * GET BENEFICIARY ADDRESS
     *
     */
    function getBeneficiaryAddress() public view returns (address) {
        return beneficiaryAddress;
    }

    /**
     * GET EQUITY TRANCHE ADDRESS
     *
     */
    function getEquityTrancheAddress() public view returns (address) {
        return equityTrancheAddress;
    }

    /**
     * GET STAKEHOLDER BALANCE
     *
     */
    function getStakeholderBalance(address userWallet) public view returns (uint);



    /**
     * GET GROUP
     *
     */
    function getGroup(string name) public view returns (Group);

    /**
     * GET TRANCHE
     */
    function getTranche(string territory, uint index) public view returns (Tranche);
}